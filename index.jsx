import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import App from './page/app';
import Home from './page/home';
import Detail from './page/detail';
import Search from './page/search';
import Resources from './page/resources';
import About from './page/about';
import Authorize from './page/authorize';
import './index.scss';

render(
  (
    <Router history={browserHistory} onUpdate={() => window.scrollTo(0, 0)}>
      <Route path="/authorize" component={Authorize} />
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="/detail/:articleID" component={Detail} />
        <Route path="/search" component={Search} />
        <Route path="/resource" component={Resources} />
        <Route path="/about" component={About} />
        <Route path="/date/:dateTime" component={Home} />
        <Route path="/tag/:tagName" component={Home} />
        <Route path="/(:typeID/:typeName)" component={Home} />
      </Route>
    </Router>
  ), document.getElementById('J-container'),
);
