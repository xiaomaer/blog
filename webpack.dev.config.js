//开发环境中通用的配置
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlwebpackPlugin = require('html-webpack-plugin');
const HappyPack = require('happypack'); //让loader可以多进程去处理文件
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({
    size: os.cpus().length
});
const CleanWebpackPlugin = require('clean-webpack-plugin'); //清空文件夹
const DashboardPlugin = require('webpack-dashboard/plugin'); //webpack日志面板

module.exports = {
    entry: {
        index: './index.jsx',
        vendor: ['react', 'react-dom', 'react-router', 'flux', 'prop-types']
    },
    output: {
        path: 'build',
        publicPath: '/build/',
        filename: '[name].js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
       /*  preloaders: [{
            test: /\.jsx?$/,
            loader: "eslint-loader",
            include: '/page/',
            exclude: /node_modules/,
        }], */
        loaders: [
            /* {
                test: /\.jsx?$/,
                loader: "eslint-loader",
                enforce: "pre",
                exclude: /node_modules/,
            }, */
            {
                test: /\.jsx?$/, //js结尾
                exclude: /node_module/, //排除node_module目录下面的js文件
                loader: 'HappyPack/loader?id=jsHappy', //ES6转换为ES5
            }, {
                // sass编译成css，需要style-loader、css-loader、sass-loader和node-sass四个npm包
                // node-sass:用于处理scss文件中通过@import导入的文件 scss编译成css，并压缩css代码
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader!sass-loader") //打包后样式是css文件，通过<link>标签引入
            }
        ]
    },
    //配置本地代理服务器
    devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true,
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("development")
            }
        }),
        new DashboardPlugin(),
        new CleanWebpackPlugin(['build'], {
            root: __dirname,
            　　　　　　　　　　 //根目录
            verbose: true,
            　　　　　　　　　　 //开启在控制台输出信息
            dry: false　　　　　　　　　　 //启用删除文件
        }),
        //热更新
        new webpack.HotModuleReplacementPlugin(),
        // 配置提取出的样式文件
        new ExtractTextPlugin('[name].css'),
        // 提取react公共基础库
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),
        //让loader可以多进程去处理文件
        new HappyPack({
            id: 'jsHappy',
            threadPool: happyThreadPool,
            loaders: [{
                path: 'babel',
                query: {
                    cacheDirectory: '.webpack_cache',
                }
            }]
        }),
        new HtmlwebpackPlugin({
            filename: '../index.html', //filename配置的html文件目录是相对于webpackConfig.output.path路径而言的，不是相对于当前项目目录结构的。
            template: './template/index.html', //html文件的模板
        }),
        new webpack.SourceMapDevToolPlugin({
            filename: '[name].js.map',
            exclude: ['vendor.js']
        }),
    ]
}