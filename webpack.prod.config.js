//生产环境中通用的配置
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlwebpackPlugin = require('html-webpack-plugin');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');//并行运行uglifyjs插件
const HappyPack = require('happypack');//让loader可以多进程去处理文件
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length });
const CleanWebpackPlugin = require('clean-webpack-plugin');//清空文件夹
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');//脚本内联到html
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;//打包模块分析

module.exports = {
    entry: {
        index: './index.jsx',
        vendor: ['react', 'react-dom', 'react-router', 'flux', 'prop-types'],
    },
    output: {
        path: 'build',
        publicPath: '/build/',
        filename: '[name].[chunkhash:8].js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/, //js结尾
                exclude: /node_module/, //排除node_module目录下面的js文件
                loader: 'HappyPack/loader?id=jsHappy', //ES6转换为ES5
            }, {
                // sass编译成css，需要style-loader、css-loader、sass-loader和node-sass四个npm包
                // node-sass:用于处理scss文件中通过@import导入的文件 scss编译成css，并压缩css代码
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader!sass-loader") //打包后样式是css文件，通过<link>标签引入
            }
        ]
    },
    plugins: [
        //使用下面的方法设置production环境，不然发布后，提示使用的不是production版本的react的
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new CleanWebpackPlugin(['build'], {
            root: __dirname,       　　　　　　　　　　//根目录
            verbose: true,        　　　　　　　　　　//开启在控制台输出信息
            dry: false        　　　　　　　　　　//启用删除文件
        }),
        // 配置提取出的样式文件
        new ExtractTextPlugin('[name].[contenthash:8].css'),
        // 提取react公共基础库
        new webpack.optimize.CommonsChunkPlugin({
            name: ['vendor', 'manifest']
        }),
        //js 内联
        new InlineManifestWebpackPlugin({
            name: 'webpackManifest'
        }),
        //让loader可以多进程去处理文件
        new HappyPack({
            id: 'jsHappy',
            threadPool: happyThreadPool,
            loaders: [{
                path: 'babel',
                query: {
                    cacheDirectory: '.webpack_cache',
                }
            }]
        }),
        //并行运行uglifyjs插件
        new ParallelUglifyPlugin({
            cacheDir: '.cache/',
            uglifyJS: {
                output: {
                    comments: false,
                    beautify: false,
                },
                compress: {
                    warnings: false,
                    drop_debugger: true,
                    drop_console: true,
                    // 内嵌定义了但是只用到一次的变量
                    collapse_vars: true,
                    // 提取出出现多次但是没有定义成变量去引用的静态值
                    reduce_vars: true,
                }
            }
        }),
        //该插件的作用是为组件分配id，通过这个插件webpack会分析使用频率最多的模块，并为他们分配最小的id，id越小表示模块被找到的速度会更快
        //new webpack.optimize.OccurenceOrderPlugin(),//开启后，将会影响html中引用js文件的顺序
        //根据已有的html文件生成html文件
        new HtmlwebpackPlugin({
            filename: '../index.html', //输出文件【注意：这里的根路径是module.exports.output.path】
            template: './template/index.html', //html文件的模板
            minify: { //压缩HTML文件
                removeComments: true, //移除HTML中的注释
                collapseWhitespace: true, //删除空白符与换行符
                minifyCSS: true, //压缩内联css
                minifyJS: true //压缩script标签中的js
            }
        }),
        new BundleAnalyzerPlugin(),
    ]
}
