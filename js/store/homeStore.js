/**
 * Created by xiaoma on 2016/12/21.
 */
import { ReduceStore } from 'flux/utils';
import Dispatcher from '../dispatcher/dispatcher';

class HomeStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }
  getInitialState() {
    return {
      bigTypes: [],
      newArticles: [],
      hotArticles: [],
      dateList: [],
      tagList: [],
      articles: {},
      articleDetail: {
        article: {},
        catName: '',
        prev: null,
        next: null,
      },
      comments: [],
      commentIndex: -1,
      commentState: false,
      user: {},
      isLogin: false,
      isLoading: false,
    };
  }
  reduce(state, action) {
    switch (action.actionType) {
      case 'ARTICLE_TYPE':
        const newState = {
          ...state,
        };
        const {
          data,
        } = action;
        const bigTypes = data[0];
        for (let i = 0, blen = bigTypes.length; i < blen; i++) {
          for (let j = 1, len = data.length; j < len; j++) {
            if (data[j][0].ParentID === bigTypes[i].ID) {
              bigTypes[i].subTypes = data[j];
              break;
            }
          }
        }
        newState.bigTypes = bigTypes;
        localStorage.setItem('ARTICLE_TYPE', JSON.stringify(bigTypes));
        return newState;
      case 'NEW_ARTICLE_LIST':
        return {
          ...state,
          newArticles: action.data,
        };
      case 'HOT_ARTICLE_LIST':
        return {
          ...state,
          hotArticles: action.data,
        };
      case 'ARTICLE_DATE_LIST':
        return {
          ...state,
          dateList: action.data,
        };
      case 'ARTICLE_TAG_LIST':
        return {
          ...state,
          tagList: action.data,
        };
      case 'ARTICLE_LIST':
        const newStateData = {
          ...state,
          articles: action.data,
          isLoading: false,
        };
        newStateData.articles.page_total = Math.ceil(action.data.total / 20);
        return newStateData;
      case 'ARTICLE_DETAIL':
        return {
          ...state,
          articleDetail: action.data,
          isLoading: false,
        };
      case 'ARTICLE_COMMENT_LIST':
        return {
          ...state,
          ...action.data,
        };
      case 'CHANGE_COMMENT_INDEX':
        return {
          ...state,
          commentIndex: action.data,
        };
      case 'STORAGE_DATA':
        return {
          ...state,
          ...action.data,
        };
      case 'UPDATE_LOGIN_STATE':
        return {
          ...state,
          isLogin: true,
        };
      case 'USER_INFO':
        return {
          ...state,
          user: action.data,
        };
      case 'UPDATE_COMMENT_STATE':
        return {
          ...state,
          commentState: action.data,
        };
      case 'GET_DETAIL_STATE':
        return {
          ...state,
          isLoading: action.data,
        };
      default:
        return state;
    }
  }
}
export default new HomeStore();
