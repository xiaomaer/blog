/**
 * Created by xiaoma on 2016/12/21.
 */
import pageActions from '../action/homeActions';
import $ from '../common/ajax';
import Toast from '../common/toast';

class API {
  /**
   * 获取文章类型
   * @param {*} userID ：用户ID，默认0，1:他，2:我
   */
  getArticleAllTypes() {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Category',
      data: {
        userID: 2, // 0:全部，1:他，2:我
      },
      success(data) {
        pageActions.getArticleTypes(data);
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   * 获取文章时间归档列表
   */
  getDateList() {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Date',
      data: {
        userID: 2,
      },
      success(data) {
        localStorage.setItem('ARTICLE_DATE_LIST', JSON.stringify(Object.keys(data)));
        pageActions.getDateList(Object.keys(data));
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   * 获取文章关键字列表
   */
  getTagList() {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Tag',
      data: {
        userID: 2,
      },
      success(data) {
        localStorage.setItem('ARTICLE_TAG_LIST', JSON.stringify(data));
        pageActions.getTagList(data);
      },
      error(error) {
        console.log(error);
      },
    });
  }
  // 获取最新文章列表
  getNewArticleList() {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Article',
      data: {
        userID: 2, // int,用户ID
        cid: 0,
        query: '',
        page: 1,
        size: 10, // int,每页个数
        date: '',
        tag: '',
      },
      success(data) {
        localStorage.setItem('NEW_ARTICLE_LIST', JSON.stringify(data));
        pageActions.getNewArticleList(data.rows);
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   * 获取热门文章列表
   */
  getHotArticleList() {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Hot',
      data: {
        userID: 2, // int,用户ID
      },
      success(data) {
        localStorage.setItem('HOT_ARTICLE_LIST', JSON.stringify(data.slice(0, 10)));
        pageActions.getHotArticleList(data.slice(0, 10));
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   *获取文章列表
   * @param {*} cid ：int,分类ID,为0表示不指定分类，获取所有的文章
   * @param {*} query ：string, 查询字符串
   * @param {*} page_num ：int,当前页码
   * @param {*} type_name ：string,文章类型或查询字段
   */
  getArticleList(params) {
    pageActions.getLoadingState();
    const typeName = params.type_name ?
      params.type_name :
      '全部文章';
    // delete params.typeName;
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Article',
      data: params,
      success(data) {
        data.type_name = typeName;
        pageActions.getArticleList(data);
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   * 用来获取文章详细信息及上一篇和下一篇文章简介
   * @param {*} ID :文章ID
   * @param {*} url :当前文章的URL
   */
  getArticleDetail(ID, url) {
    pageActions.getLoadingState();
    const self = this;
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/Api/Article',
      data: {
        userID: 2,
        id: ID,
      },
      success(data) {
        pageActions.getArticleDetail(data);
        // 获取文章在畅言中的topic_id，然后获取文章评论列表
        self.getArticleTopicId(url, ID, data.article.Title);
      },
      error(error) {
        console.log(error);
      },
    });
  }

  /**
   * 第三方登录后，通过code获取畅言用户token
   * @param {*} code
   * @param {*} callback
   */
  getAccessToken(code, callback) {
    $.ajax({
      type: 'post',
      url: 'https://changyan.sohu.com/api/oauth2/token',
      data: {
        client_id: 'cysaWS3bo',
        client_secret: '51d9c191b36e46cd4fe6939b9a214515',
        grant_type: 'authorization_code',
        code,
        redirect_uri: 'http://mia.dobit.top/authorize',
      },
      success(data) {
        // 获取token，通过cookie存储，然后用于发表评论
        callback && callback(data);
      },
      error(error) {
        console.log(error);
      },
    });
  }
  /**
   * topic/load：获取对应URL文章的topic_id，用于获取文章评论列表和发表、回复评论
   * 接口不需要登录
   * @param {string} url:文章URL
   */
  getArticleTopicId(url, articleID, articleTitle) {
    const self = this;
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/api/comment',
      data: {
        topic_url: url,
        topic_title: articleTitle,
        topic_source_id: articleID,
      },
      success(data) {
        const res = JSON.parse(data);
        const {
          topic_id,
        } = res;
        // 存储topic_id,用于获取文章评论列表、发表和回复评论
        sessionStorage.setItem('PAGE_TOPIC_ID ', topic_id);
        // 获取文章评论列表
        self.getArticleComments(topic_id);
      },
      error(error) {
        console.log(JSON.parse(error));
      },
    });
  }
  /**
   * 调用topic/comments接口：用于获取文章评论列表
   * 接口不需要登录
   * @param {number} topic_id: 文章topic_id
   */
  getArticleComments(topic_id) {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/api/comment',
      data: {
        topic_id,
        page_size: 30,
        page_no: 1,
      },
      success(data) {
        pageActions.getCommentList(JSON.parse(data));
      },
      error(error) {
        console.log(JSON.parse(error));
      },
    });
  }
  /**
   * 用户发表或回复评论
   * 接口需要登录
   * @param {number} topic_id :文章ID
   * @param {string} content：评论或回复内容
   * @param {number} reply_id ：回复的评论ID，非必需
   * @param {string} token ：登录token
   */
  submitComment(topic_id, content, reply_id, token) {
    // 封装后的接口请求不成功
    /* $.ajax({
            type: 'post',
            url: ' http://dobit.top/api/comment',
            data: {
                topic_id: topic_id,
                content: content,
                reply_id: reply_id,
                access_token: token
            },
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
                console.log(error);
            }
        }) */
    pageActions.updateCommentState(false);
    let params = `client_id=cysaWS3bo&topic_id=${topic_id}&content=${content}&access_token=${token}`;
    if (reply_id) {
      params += `&reply_id=${reply_id}`;
    }
    $.post('http://changyan.sohu.com/api/2/comment/submit', params, (data) => {
      const commentId = data.id;
      if (commentId) {
        pageActions.changeCommentIndex(-1);
        pageActions.updateCommentState(true);
        Toast.show({
          message: '评论成功，审核后将会展示在评论列表',
          duration: 2000,
        });
      } else {
        Toast.show({
          message: '评论失败，请稍后重试',
        });
      }
    }, (error) => {
      Toast.show({
        message: error || '评论失败，请稍后重试',
      });
    });
  }
  /**
   * 获取当前登录用户的信息
   * @param {*} token:当前用户token
   */
  getCurrUserInfo(token) {
    $.ajax({
      type: 'get',
      url: ' http://dobit.top/api/comment/user',
      data: {
        access_token: token,
      },
      success(data) {
        pageActions.getCurrUserInfo(JSON.parse(data));
      },
      error(error) {
        console.log(JSON.parse(error));
      },
    });
  }
}
export default new API();
