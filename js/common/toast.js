export default class Toast {
  constructor() {
    this.timer = null;
  }
  /* 弹出提示
   *options:对象，{message:'string',duration:'number'}
   * message：必填，toast显示的提示消息。
   * duration：选填，提示框消失的时间，毫秒为单位，默认值为1500ms。 */
  static show(options) {
    const msg = options.message || '';
    const duration = options.duration || 1500;
    // 如果页面有toast显示时，再显示另外一个toast，要先关闭timer,不然toast小时时间不对
    if (this.timer) {
      clearInterval(this.timer);
    }
    if (!this.elemNode) {
      const elemNode = document.createElement('div');
      document.body.appendChild(elemNode);
      this.elemNode = elemNode;
    }
    this.elemNode.innerHTML = msg;
    this.elemNode.className = 'toast-show';
    this.timer = setTimeout(() => {
      this.elemNode.className = 'toast-show toast-hide';
    }, duration);
  }
}
