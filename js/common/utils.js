/**
 * 设置cookie
 * @param {string} name :cookie名称
 * @param {*} value ：cookie值
 * @param {number} value :几天后过期
 */
export function setCookie(name, value, days) {
  const exp = new Date();
  exp.setDate(exp.getDate() + days);
  // exp.setTime(exp.getTime() + days * 24 * 60 * 60 * 1000);
  document.cookie = `${name}=${escape(value)};path=/;expires=${exp.toGMTString()}`;
}

// 读取cookies
export function getCookie(name) {
  const reg = new RegExp(`(^| )${name}=([^;]*)(;|$)`);
  const arr = document.cookie.match(reg) || [];
  if (arr.length !== 0) {
    return unescape(arr[2]);
  }
  return null;
}

// 删除cookies
export function delCookie(name) {
  const exp = new Date();
  exp.setTime(exp.getTime() - 1);
  const cval = getCookie(name);
  if (cval != null) {
    document.cookie = `${name}=${cval};expires=${exp.toGMTString()}`;
  }
}

/*
 timeStr:时间，格式可为："September 16,2016 14:15:05、
 "September 16,2016"、"2016/09/16 14:15:05"、"2016/09/16"、
 '2014-04-23T18:55:49'和毫秒
 dateSeparator：年、月、日之间的分隔符，默认为"-"，
 timeSeparator：时、分、秒之间的分隔符，默认为":"
 */
export function getFormatDate(timeStr, dateSeparator, timeSeparator) {
  dateSeparator = dateSeparator || '-';
  timeSeparator = timeSeparator || ':';
  const date = new Date(timeStr);
  const year = date.getFullYear(); // 获取完整的年份(4位,1970)
  const month = date.getMonth(); // 获取月份(0-11,0代表1月,用的时候记得加上1)
  const day = date.getDate(); // 获取日(1-31)
  const hour = date.getHours(); // 获取小时数(0-23)
  const minute = date.getMinutes(); // 获取分钟数(0-59)
  const seconds = date.getSeconds(); // 获取秒数(0-59)
  const Y = year + dateSeparator;
  const M = ((month + 1) > 9 ?
    (month + 1) :
    (`0${month + 1}`)) + dateSeparator;
  const D = `${day > 9 ? day : (`0${day}`)} `;
  const h = (hour > 9 ?
    hour :
    (`0${hour}`)) + timeSeparator;
  const m = (minute > 9 ?
    minute :
    (`0${minute}`)) + timeSeparator;
  const s = (seconds > 9 ?
    seconds :
    (`0${seconds}`));
  const formatDate = Y + M + D + h + m + s;
  return formatDate;
}
/**
 * 验证邮箱是否合法
 * @param {string} email
 */
export function checkEmail(email) {
  const emailReg = /^[A-Za-zd]+([-_.][A-Za-zd]+)*@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$/;
  return emailReg.test(email);
}
