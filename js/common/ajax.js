const ajax = {
  get(url, success, error) {
    const xhr = new XMLHttpRequest();
    xhr.open('get', url, true);
    // xhr.withCredentials = true;
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        const response = xhr.responseText ? JSON.parse(xhr.responseText) : {};
        if (xhr.status === 200) {
          success && success(response);
        } else {
          error && error(response);
        }
      }
    };
    xhr.send();
  },
  post(url, params, success, error) {
    const xhr = new XMLHttpRequest();
    xhr.open('post', url, true);
    // xhr.withCredentials = true;
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        const response = xhr.responseText ? JSON.parse(xhr.responseText) : {};
        if (xhr.status === 200) {
          success && success(response);
        } else {
          error && error(response);
        }
      }
    };
    xhr.send(params);
  },
  ajax(options) {
    const {
      type,
      url,
      data,
      contentType,
      success,
      error,
    } = options;
    const xhr = new XMLHttpRequest();
    const method = type.toLowerCase();
    if (method === 'get') {
      // 处理字符串
      const arr = [];
      for (const param in data) {
        arr.push(`${param}=${data[param]}`);
      }
      xhr.open(type, `${url}?${arr.join('&')}`, true);
      xhr.send();
    } else if (method === 'post') {
      xhr.open(type, url, true);
      if (contentType === 'application/json') {
        xhr.setRequestHeader('content-type', 'application/json');
        xhr.send(JSON.stringify(data)); // json格式传参数
      } else {
        const formData = new FormData();
        for (const key in data) {
          formData.append(key, data[key]);
        }
        xhr.send(formData);
      }
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        const response = xhr.responseText ? JSON.parse(xhr.responseText) : {};
        if (xhr.status === 200) {
          success && success(response);
        } else {
          error && error(response);
        }
      }
    };
  },
};
export default ajax;
