/**
 * Created by xiaoma on 2016/12/21.
 */
import { dispatch } from '../dispatcher/dispatcher';

const PageActions = {
  // 获取文章类型
  getArticleTypes(data) {
    dispatch({ actionType: 'ARTICLE_TYPE', data });
  },
  // 获取文章列表
  getArticleList(data) {
    dispatch({ actionType: 'ARTICLE_LIST', data });
  },
  // 获取最新文章列表
  getNewArticleList(data) {
    dispatch({ actionType: 'NEW_ARTICLE_LIST', data });
  },
  // 获取最热文章列表
  getHotArticleList(data) {
    dispatch({ actionType: 'HOT_ARTICLE_LIST', data });
  },
  // 获取文章详情
  getArticleDetail(data) {
    dispatch({ actionType: 'ARTICLE_DETAIL', data });
  },
  // 获取文章评论列表
  getCommentList(data) {
    dispatch({ actionType: 'ARTICLE_COMMENT_LIST', data });
  },
  // 修改在哪里显示评论框
  changeCommentIndex(data) {
    dispatch({ actionType: 'CHANGE_COMMENT_INDEX', data });
  },
  // 获取文章时间归档列表
  getDateList(data) {
    dispatch({ actionType: 'ARTICLE_DATE_LIST', data });
  },
  // 获取文章关键字列表
  getTagList(data) {
    dispatch({ actionType: 'ARTICLE_TAG_LIST', data });
  },
  // 获取缓存数据
  getStorageData(data) {
    dispatch({ actionType: 'STORAGE_DATA', data });
  },
  // 更新登录
  updataLoginState() {
    dispatch({ actionType: 'UPDATE_LOGIN_STATE' });
  },
  // 获取当前登录用户的信息
  getCurrUserInfo(data) {
    dispatch({ actionType: 'USER_INFO', data });
  },
  // 评论成功，更新评论
  updateCommentState(data) {
    dispatch({ actionType: 'UPDATE_COMMENT_STATE', data });
  },
  // 请求进行状态
  getLoadingState() {
    dispatch({ actionType: 'GET_DETAIL_STATE', data: true });
  },
};
export default PageActions;
