/**
 * 登录对话框
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class LoginDialog extends PureComponent {
  static propTypes = {
    isShow: PropTypes.bool,
    hideDialog: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
  }
  static defaultProps = {
    isShow: false,
  }
  render() {
    const { isShow, hideDialog, login } = this.props;
    if (isShow) {
      return (
        <div className="login">
          <div className="fixed_position login_mask" />
          <div className="fixed_position login_dialog">
            <h1 className="blog_name">MiaBlog</h1>
            <div className="login_title"><span>请选择登录方式</span></div>
            <ul className="login_methods">
              <li className="login_method weibo" onClick={() => login(2)} />
              <li className="login_method qq" onClick={() => login(3)} />
              <li className="login_method baidu" onClick={() => login(7)} />
              <li className="login_method wx" onClick={() => login(13)} />
            </ul>
            <div className="close_login_dialog" onClick={hideDialog}><i className="iconfont icon-cuo" /></div>
          </div>
        </div>
      );
    }
    return null;
  }
}
