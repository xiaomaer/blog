import React from 'react';
import PropTypes from 'prop-types';
import { getFormatDate } from '../../common/utils';

const ReplyList = (props) => {
  const { reply } = props;
  return (
    <ul className="replay_comment_list">
      <li className="comment_item" key={reply.comment_id}>
        <div className="comment_content">{reply.content}</div>
        <div className="comment_other">
          <span><i className="iconfont icon-yonghu" />{reply.passport.nickname}</span>
          <span>{getFormatDate(reply.create_time)}</span>
        </div>
      </li>
    </ul>
  );
};

ReplyList.propTypes = {
  reply: PropTypes.object.isRequired,
};
export default ReplyList;
