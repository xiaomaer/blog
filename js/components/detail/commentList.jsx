/**
 * 评论列表
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getFormatDate } from '../../common/utils';
import pageActions from '../../action/homeActions';
import Comment from './comment';
import Reply from './replyList';

export default class CommentList extends PureComponent {
  static propTypes = {
    index: PropTypes.number,
    data: PropTypes.array,
    userInfo: PropTypes.object,
    onSubmit: PropTypes.func,
  }
  static defaultProps = {
    index: 0,
    data: [],
    userInfo: {},
    onSubmit: () => { },
  }
  replyComment = (e) => {
    const commentIndex = e.target.dataset.index;
    // 显示评论框
    pageActions.changeCommentIndex(parseInt(commentIndex, 10));
  }
  render() {
    const {
      index, data, userInfo, onSubmit,
    } = this.props;
    const commentList = data.map((item, currIndex) => {
      if (item.reply_id === 0) { // 没有回复的评论
        const { passport } = item;
        return (
          <li key={currIndex}>
            <div className="comment_info">
              <div className="user_img_container">
                <img src={passport.img_url} className="user_img" alt="" />
              </div>
              <div className="comment_item">
                <div className="comment_content">{item.content}</div>
                <div className="comment_other">
                  <span>{passport.nickname}</span>
                  <span>{getFormatDate(item.create_time)}</span>
                  <span
                    className="comment_reply"
                    data-index={currIndex}
                    onClick={this.replyComment}
                  >回复
                  </span>
                </div>
              </div>
            </div>
            {currIndex === index && <Comment commentID={item.comment_id} onSubmit={onSubmit} user={userInfo.nickname} />}
          </li>
        );
      } // 有回复的评论
      const comment = item.comments[0];
      return (
        <li key={currIndex}>
          <div className="comment_info">
            <div className="user_img_container">
              <img src={comment.passport.img_url} className="user_img" alt="" />
            </div>
            <div className="comment_item">
              <div className="comment_content">{comment.content}</div>
              <div className="comment_other">
                <span>{comment.passport.nickname}</span>
                <span>{getFormatDate(comment.create_time)}</span>
                <span
                  className="comment_reply"
                  data-index={currIndex}
                  onClick={this.replyComment}
                >回复
                </span>
              </div>
            </div>
          </div>
          {currIndex === index && <Comment commentID={comment.comment_id} onSubmit={onSubmit} user={userInfo.nickname} />}
          {item.comments.length !== 0 && <Reply reply={item} />}
        </li>
      );
    });
    return (
      <ul className="comment_list">
        {commentList}
      </ul>
    );
  }
}
