import React from 'react';
import PropTypes from 'prop-types';

const Loading = (props) => {
  if (props.isLoading) {
    return (
      <div className="spinner">
        <div className="rect1" />
        <div className="rect2" />
        <div className="rect3" />
        <div className="rect4" />
        <div className="rect5" />
      </div>
    );
  }
  return null;
};
Loading.propTypes = {
  isLoading: PropTypes.bool,
};
Loading.defaultProps = {
  isLoading: false,
};

export default Loading;
