/* 文章详情 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

export default class Detail extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      url: window.location.href,
    };
  }
  render() {
    const { data } = this.props;
    const { article, prev: prev_article, next: next_article } = data;
    const tags = article.Keywords && article
      .Keywords
      .split('、')
      .map((item, index) => (
        <li className="article_tag" key={index}>{item}</li>
      ));
    return (
      <div className="detail">
        <h1 className="detail_name">{article.Title}</h1>
        <div className="detail_tags">
          <Link to={`/${article.CatID}/${data.catName}`} className="article_type">{data.catName}</Link>
          <span className="detail_tag">
            {article.CreateDateTime && <i className="iconfont icon-dateto" />}
            <span>{article.CreateDateTime && article
              .CreateDateTime
              .substr(0, 10)}
            </span>
          </span>
          <span className="detail_tag">
            {article.Clicks && <i className="iconfont icon-liulan" />}
            <span>{article.Clicks}</span>
          </span>
        </div>
        <div
          className="detail_content"
          dangerouslySetInnerHTML={{
            __html: article.Content,
          }}
        />
        {tags && (
          <div className="article_tags">
            <span className="description">文章标签</span>
            <ul>{tags}</ul>
          </div>
        )}
        {article.Content && (
          <div className="reprinted_article">
            <ul className="reprinted_tips">
              <li>文章地址：<a href={this.state.url}>{this.state.url}</a></li>
              <li>欢迎转载，转载请标明原文出处哦；如有问题，欢迎反馈哦。</li>
            </ul>
          </div>
        )}

        {(prev_article || next_article) &&
          <div className="switch_article">
            {prev_article && (
              <Link to={`/detail/${prev_article.ID}`}>
                <span className="prev_article">上一篇</span>
                <span className="article_title">{prev_article.Title}</span>
              </Link>
            )}
            {next_article && (
              <Link to={`/detail/${next_article.ID}`}>
                <span className="next_article">下一篇</span>
                <span className="article_title">{next_article.Title}</span>
              </Link>
            )}
          </div>}
      </div>
    );
  }
}
