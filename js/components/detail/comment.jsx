import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import pageActions from '../../action/homeActions';

export default class Comment extends PureComponent {
  static propTypes = {
    commentState: PropTypes.bool.isRequired,
    user: PropTypes.string,
    userInfo: PropTypes.object,
    commentID: PropTypes.number,
    onSubmit: PropTypes.func,
  }
  static defaultProps = {
    user: '',
    userInfo: {},
    commentID: 0,
    onSubmit: () => { },
  }
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
    };
  }
  componentWillReceiveProps(nextProps) {
    // 评论成功，清空评论内容
    if (nextProps.commentState === true && this.props.commentState === false) {
      this.setState({
        comment: '',
      });
    }
  }
  changeComment = () => {
    this.setState({
      comment: this.refs.commentContent.value,
    });
  }
  submitComment = (e) => {
    e.preventDefault();
    this.props.onSubmit && this.props.onSubmit(this.state.comment, this.props.commentID);
  }
  cancelReply = () => {
    // 关闭评论内容下的回复评论框，显示默认的发表评论
    pageActions.changeCommentIndex(-1);
  }
  render() {
    const { commentID, user, userInfo } = this.props;
    return (
      <div className="comment">
        {commentID !== 0 &&
          <div className="reply_info">
            <span>{user ? `${user} 回复说:` : '评论回复'}</span>
            <button type="button" className="cancel_reply" onClick={this.cancelReply}>取消回复</button>
          </div>}
        {userInfo && userInfo.nickname &&
          <div className="user_info">
            <img src={userInfo.img_url} className="user_image" alt="" />
            <span className="user_name">{`${userInfo.nickname} 说：`}</span>
          </div>}
        <form onSubmit={this.submitComment}>
          <div className="comment_input comment_value">
            <textarea ref="commentContent" placeholder="Hey！随便留个脚印纪念哦 ^-^" required onChange={this.changeComment} value={this.state.comment} />
          </div>
          <div className="comment_btn">
            <button type="submit" className="comment_submit"><i className="iconfont icon-xinxi" />发表评论</button>
          </div>
        </form>
      </div>

    );
  }
}
