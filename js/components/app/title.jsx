/* 左右侧边栏标题 */

import React from 'react';
import PropTypes from 'prop-types';

const Title = (props) => {
  const { title, icon } = props;
  return (
    <div className="title">
      <h1 className="title_name">
        {icon ? (<i className={`iconfont ${icon}`} />) : ''}
        {title}
      </h1>
    </div>
  );
};
Title.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
};
Title.defaultProps = {
  icon: '',
};
export default Title;
