// 子菜单
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const SubMenus = (props) => {
  const { data } = props;
  const submenus = data.map((item, index) => (
    <li className="subtype_item" key={`s_${index}`}>
      <Link to={`/${item.ID}/${item.CatName}`} className="type_link" activeClassName="type_link_active">{`${item.CatName} (${item.ArticleCount})`}</Link>
    </li>
  ));
  return (
    <ul className="type_list">
      {submenus}
    </ul>
  );
};
SubMenus.propTypes = {
  data: PropTypes.array,
};
SubMenus.defaultProps = {
  data: [],
};
export default SubMenus;
