/* 顶部导航条 */
/**
 * Created by xiaoma on 2016/12/20.
 */
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import PropTypes from 'prop-types';

export default class Nav extends Component {
  static propTypes = {
    query: PropTypes.string,
    types: PropTypes.array,
  }
  static defaultProps = {
    query: '',
    types: [],
  }
  constructor(props) {
    super(props);
    this.state = {
      isShowMenus: false,
      isShowSearch: false,
      queryStr: '',
      showStr: '',
    };
  }
  componentDidMount() {
    // this.refs.queryStr.focus(); //页面上有两个相同值的ref时，focus方法获取焦点时，只有最后一个ref对应的input输入框可获取焦点
    // 刷新和重定向页面时，如果该页面是查询页面，初始化搜索框的值和状态
    const { query } = this.props;
    if (query) {
      this.setState({ showStr: query, isShowSearch: true });
    }
  }
  componentWillReceiveProps(nextProps) {
    // url为非搜索页面时，重新初始化搜索框
    if (nextProps.query === '') {
      this.setState({ queryStr: '', showStr: '', isShowSearch: false });
    }
  }
  // 移动端，切换是否显示左侧菜单
  toggleMenus = () => {
    this.setState({
      isShowMenus: !this.state.isShowMenus,
    });
  }
  // 移动端，点击菜单项后，关闭左侧菜单
  handleLink = (e) => {
    if (e.target.tagName.toLowerCase() === 'a') {
      this.setState({ isShowMenus: false });
    }
  }
  // 移动端，点击遮罩层时，关闭左侧菜单
  closeMenus = () => {
    this.setState({ isShowMenus: false });
  }
  // 显示搜索框，并且input获取焦点
  showSearchInput = () => {
    this.setState({ isShowSearch: true }, () => {
      // 隐藏的元素，需要显示后，重新调用focus方法获取焦点哦
      this.refs.queryStr.focus();
    });
  }
  hideSearchInput = () => {
    this.setState({ isShowSearch: false });
  }
  // 如果输入框内容为空，并且input失去焦点，隐藏搜索框
  hasChangeQueryStr = (e) => {
    if (e.target.value === '') {
      this.setState({ isShowSearch: false });
    }
  }
  // 查询文章
  searchArticles = (e) => {
    e.preventDefault();
    const queryStr = this.refs.queryStr.value;
    // 查询字符串是否发生了变化，dom onchange事件。没发生变化或者查询字符串为空，不做任何处理；发生变化进行查询
    if (this.state.queryStr === queryStr || queryStr === '') { return; }
    const path = `/search?query=${queryStr}`;
    browserHistory.push(path);
    this.setState({ queryStr });
  }
  // 实时更新查询字符串的值
  handleChangeQueryStr = (e) => {
    this.setState({ showStr: e.target.value });
  }
  render() {
    const { types } = this.props;
    const menus = types.map((item, index) => (
      <li className="nav_submenu" key={index}>
        <Link
          to={`/${item.ID}/${item.CatName}`}
          className="nav_sublink"
          activeClassName="nav_sublink_active"
        >{item.CatName}
        </Link>
      </li>
    ));
    const { isShowMenus, isShowSearch } = this.state;
    const showMobileMenus = isShowMenus
      ? ' nav_menus_show'
      : '';
    const style = isShowSearch
      ? {
        display: 'none',
      }
      : {};
    return (
      <header>
        {/* pc端导航条 */}
        <div className="header header_bg_white only_pc_show">
          <div className="nav_PC page_container">
            <Link to="/" className="nav_logo">MiaBlog</Link>
            <ul className="nav_menus">
              <li className="nav_menu">
                <Link to="/" className="nav_link">全部文章</Link>
              </li>
              <li className="nav_menu">
                <Link className="nav_link nav_link_no">文章类型</Link>
                <ul className="nav_submenus">
                  {menus}
                </ul>
              </li>
              <li className="nav_menu">
                <Link to="/resource" className="nav_link" activeClassName="nav_link_active">前端资源库</Link>
              </li>
              <li className="nav_menu">
                <Link to="/about" className="nav_link" activeClassName="nav_link_active">关于我</Link>
              </li>
            </ul>
            <form
              className={`search_area${isShowSearch
                ? ' search_area_show'
                : ''}`}
              onSubmit={this.searchArticles}
            >
              <i className="iconfont icon-search" onClick={this.searchArticles} />
              <input
                type="text"
                value={this.state.showStr}
                placeholder="找不到？搜索试试..."
                className="search_input"
                onBlur={this.hasChangeQueryStr}
                ref="queryStr"
                onChange={this.handleChangeQueryStr}
              />
            </form>
            <i
              className="iconfont icon-search"
              onClick={this.showSearchInput}
              style={style}
            />
          </div>
        </div>
        {/* 移动端导航条 */}
        <div className="header header_bg_black only_mobile_show">
          <div className="nav_mobile page_container">
            <div className="nav_header">
              <div className="nav_categories" onClick={this.toggleMenus}>
                <i className="iconfont icon-fenlei" />
              </div>
              <div className="nav_logo">MiaBlog</div>
              <div className="nav_search">
                {isShowSearch
                  ? (
                    <i className="iconfont icon-wrong" onClick={this.hideSearchInput} />
                  )
                  : (
                    <i className="iconfont icon-search" onClick={this.showSearchInput} />
                  )}
              </div>
            </div>
            <form
              className={`search_area${isShowSearch
                ? ' search_area_show'
                : ''}`}
              onSubmit={this.searchArticles}
            >
              <i className="iconfont icon-search" onClick={this.searchArticles} />
              <input
                type="text"
                value={this.state.showStr}
                placeholder="找不到？搜索试试..."
                className="search_input"
                ref="queryStr"
                onBlur={this.hasChangeQueryStr}
                onChange={this.handleChangeQueryStr}
              />
            </form>
          </div>
          <div className={`nav_menus_mask${showMobileMenus}`} onClick={this.closeMenus} />
          <ul className={`nav_menus_left${showMobileMenus}`} onClick={this.handleLink}>
            <li className="nav_menu">
              <Link to="/" className="nav_link">全部文章</Link>
            </li>
            <li className="nav_menu">
              <Link className="nav_link">文章类型</Link>
              <ul className="nav_submenus">
                {menus}
              </ul>
            </li>
            <li className="nav_menu">
              <Link to="/resource" className="nav_link" activeClassName="nav_link_active">前端资源库</Link>
            </li>
            <li className="nav_menu">
              <Link to="/about" className="nav_link" activeClassName="nav_link_active">关于我</Link>
            </li>
          </ul>
        </div>
      </header>
    );
  }
}
