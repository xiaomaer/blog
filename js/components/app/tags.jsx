/* 左侧标签列表 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Title from './title';

const Tags = (props) => {
  const { title, icon, tags } = props;
  const tagList = tags.map((item, index) => (
    <li className="tag_item" key={index}>
      <Link to={`/tag/${item}`} className="tag_link" activeClassName="tag_link_active">{item}</Link>
    </li>
  ));
  return (
    <div className="tag">
      <Title title={title} icon={icon} />
      <ul>{tagList}</ul>
    </div>
  );
};
Tags.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  tags: PropTypes.array,
};
Tags.defaultProps = {
  icon: '',
  tags: [],
};
export default Tags;
