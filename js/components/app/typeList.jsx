/* 左侧分类目录／文章归档菜单 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Title from './title';
import SubMenus from './subMenus';

const TypeList = (props) => {
  const { title, icon, types } = props;
  const menus = types.map((item, index) => (
    <li className="type_item" key={`first_${index}`}>
      <Link to={`/${item.ID}/${item.CatName}`} className="type_link" activeClassName="type_link_active">{`${item.CatName} (${item.ArticleCount})`}</Link>
      {item.subTypes && <SubMenus data={item.subTypes} />}
    </li>
  ));
  return (
    <div className="type">
      <Title title={title} icon={icon} />
      <ul className="type_list">
        {menus}
      </ul>
    </div>
  );
};
TypeList.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  types: PropTypes.array,
};
TypeList.defaultProps = {
  icon: '',
  types: [],
};

export default TypeList;
