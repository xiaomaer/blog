/* 左侧最新文章／最热文章列表 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import LazyLoad from 'react-lazy-load';
import Title from './title';

const ArticleList = (props) => {
  const {
    title, icon, data, type,
  } = props;
  const articles = data.map((item, index) => (
    <li className="article_item" key={index}>
      <Link to={`/detail/${item.ID}`} className="article_link">
        <LazyLoad>
          <img
            src={item.MainImage
              ? (`http://img.dobit.top/${item.MainImage}-blogthumb`)
              : 'http://img.dobit.top/thumb-preview.png'}// http://img.dobit.top/frontend.jpg-blogthumb
            className="article_img"
            alt=""
          />
        </LazyLoad>
      </Link>
      <div className="article_info">
        <Link to={`/detail/${item.ID}`} className="article_title">{item.Title}</Link>
        <div className="article_other">
          <Link to={`/${item.CatID}/${item.CatName}`} className="article_type">{item.CatName}</Link>
          {type === 'hot'
            ? (
              <span className="article_click">
                <i className="iconfont icon-liulan" />{item.Clicks}
              </span>
            )
            : ''}
        </div>
      </div>

    </li>
  ));
  return (
    <div className="article">
      <Title title={title} icon={icon} />
      <ul>
        {articles}
      </ul>
    </div>
  );
};

ArticleList.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  data: PropTypes.array,
  type: PropTypes.string,
};
ArticleList.defaultProps = {
  icon: '',
  data: [],
  type: '',
};

export default ArticleList;
