/* 左侧分类目录／文章归档菜单 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Title from './title';

const DateList = (props) => {
  const { title, icon, dates } = props;
  const months = ['', '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
  const dateList = dates.map((item, index) => {
    const date = item.split('-');
    const month = months[parseInt(date[1], 10)];
    return (
      <li className="type_item" key={index}>
        <Link to={`/date/${item}`} className="type_link" activeClassName="type_link_active">{`${date[0]}年${month}`}</Link>
      </li>
    );
  });
  return (
    <div className="type">
      <Title title={title} icon={icon} />
      <ul className="type_list">{dateList}</ul>
    </div>
  );
};
DateList.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  dates: PropTypes.array,
};
DateList.defaultProps = {
  icon: '',
  dates: [],
};

export default DateList;
