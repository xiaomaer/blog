/* 文章列表分页 */
import React from 'react';

const Pager = (props) => {
  const {
    pageTotal, currNum, prevPage, nextPage, changePage,
  } = props;
  const item = [];
  if (pageTotal <= 5) {
    for (let i = 1; i <= pageTotal; i++) {
      if (i === currNum) {
        item.push(<li className="pager_item pager_item_active" key={`page_${i}`} data-id={i}>{i}</li>);
      } else {
        item.push(<li className="pager_item" key={`page_${i}`} data-id={i}>{i}</li>);
      }
    }
  } else if (currNum < 5) {
    for (let i = 1; i <= 5; i++) {
      if (i === currNum) {
        item.push(<li className="pager_item pager_item_active" key={`page_${i}`} data-id={i}>{i}</li>);
      } else {
        item.push(<li className="pager_item" key={`page_${i}`} data-id={i}>{i}</li>);
      }
    }
    item.push(<li className="ellipsis" key="last">...</li>);
  } else if (currNum > (pageTotal - 3)) {
    item.push(<li className="ellipsis" key="first">...</li>);
    for (let i = (pageTotal - 4); i <= pageTotal; i++) {
      if (i === currNum) {
        item.push(<li className="pager_item pager_item_active" key={`page_${i}`} data-id={i}>{i}</li>);
      } else {
        item.push(<li className="pager_item" key={`page_${i}`} data-id={i}>{i}</li>);
      }
    }
  } else {
    item.push(<li className="ellipsis" key="first">...</li>);
    for (let i = (currNum - 2); i <= (currNum + 2); i++) {
      if (i === currNum) {
        item.push(<li className="pager_item pager_item_active" key={`page_${i}`} data-id={i}>{i}</li>);
      } else {
        item.push(<li className="pager_item" key={`page_${i}`} data-id={i}>{i}</li>);
      }
    }
    item.push(<li className="ellipsis" key="last">...</li>);
  }

  return (
    <div className="pager">
      <div className="pager_change only_mobile_show">
        <li className={`pager_item${currNum === 1 ? ' pager_item_disabled' : ''}`} onClick={prevPage}>《</li>
        <li className={`pager_item pager_next${currNum === pageTotal ? ' pager_item_disabled' : ''}`} onClick={nextPage}>》</li>
      </div>
      <ul className="only_pc_show" onClick={changePage}>
        <li className={`pager_item${currNum === 1 ? ' pager_item_disabled' : ''}`} onClick={prevPage}>《</li>
        {item}
        <li className={`pager_item${currNum === pageTotal ? ' pager_item_disabled' : ''}`} onClick={nextPage}>》</li>
      </ul>
    </div>
  );
};

export default Pager;
