/* 中间文章列表 */
import React from 'react';
import { Link } from 'react-router';
import LazyLoad from 'react-lazy-load';

const Articles = (props) => {
  const { data } = props;
  const articles = data.rows && data
    .rows
    .map(item => (
      <li className="articles_item" key={item.ID}>
        <Link to={`/detail/${item.ID}`} className="articles_img_container">
          <LazyLoad>
            <img
              src={item.MainImage
                ? (`http://img.dobit.top/${item.MainImage}-blogthumb`)
                : 'http://img.dobit.top/thumb-preview.png'}// http://img.dobit.top/frontend.jpg-blogthumb
              className="articles_img"
              alt=""
            />
          </LazyLoad>
        </Link>
        <div className="articles_info">
          <div className="articles_describe">
            <Link to={`/detail/${item.ID}`} className="articles_name">{item.Title}</Link>
            <div className="articles_content">{item.Description || item.Title}</div>
          </div>
          <div className="articles_tags">
            <Link to={`/detail/${item.ID}`} className="artticles_read">阅读全文</Link>
            <span className="articles_tag">
              <i className="iconfont icon-dateto" />
              <span>{item
                .CreateDateTime
                .substr(0, 10)}
              </span>
            </span>
            <span className="articles_tag">
              <i className="iconfont icon-liulan" />
              <span>{item.Clicks}</span>
            </span>
          </div>
        </div>
      </li>
    ));
  return (
    <div className="articles">
      <h1 className="center_title">{data.type_name}</h1>
      {data.rows && data.rows.length === 0
        ? (
          <div className="no_articles">没有符合条件的文章哦！</div>
        )
        : (
          <ul>{articles}</ul>
        )}
    </div>
  );
};

export default Articles;
