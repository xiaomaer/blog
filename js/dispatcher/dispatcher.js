/**
 * Created by xiaoma on 2016/12/21.
 */
import { Dispatcher } from 'flux';

const instance = new Dispatcher();
export default instance;
export const dispatch = instance.dispatch.bind(instance);
