# Setting up the Project

First you'll need [Node.js](https://nodejs.org) and the package manager
that comes with it: [npm](https://www.npmjs.com/).

Once you've got that working, head to the command line where we'll set
up our project.

## Run project in local environment

```
npm install
npm run dev
npm start
```

Now open up [http://localhost:8080](http://localhost:8080)

Feel free to poke around the code to see how we're using webpack and npm
scripts to run the app.


## Packages

```
npm run dev——for development(generate build files and index.html)
npm run build——for production(generate build files and index.html)
```

## JavaScript Style Guide

```
using Airbnb JavaScript Style Guide
defining rules by myself
```

