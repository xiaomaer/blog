import React from 'react';

const Resource = () => (
  <div className="resource">
    <h1 className="center_title">前端资源库</h1>
    <p className="resource_desc">这里总结了前端开发相关的一些学习资源、框架、工具、博客和书籍等，希望能给您提供一点点帮助，前端资源库会不断进行完善哦～～</p>
    <div className="resource_container">
      <div className="resource_part">
        <h2 className="resource_type">前端基础在线学习:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="http://www.w3school.com.cn/" target="_blank" rel="noopener noreferrer">W3school</a>
          </li>
          <li className="resource_item">
            <a href="https://developer.mozilla.org/zh-CN/" target="_blank" rel="noopener noreferrer">MDN</a>
          </li>
          <li className="resource_item">
            <a href="http://javascript.ruanyifeng.com/" target="_blank" rel="noopener noreferrer">JavaScript标准参考教程</a>
          </li>
          <li className="resource_item">
            <a href="http://www.css88.com/book/css/" target="_blank" rel="noopener noreferrer">CSS参考手册</a>
          </li>
          <li className="resource_item">
            <a href="http://wiki.jikexueyuan.com/list/front-end/" target="_blank" rel="noopener noreferrer">极客学院</a>
          </li>
          <li className="resource_item">
            <a href="http://www.imooc.com/course/list" target="_blank" rel="noopener noreferrer">慕课网</a>
          </li>
        </ul>
      </div>

      <div className="resource_part">
        <h2 className="resource_type">前端框架:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="https://facebook.github.io/react/" target="_blank" rel="noopener noreferrer">React</a>
          </li>
          <li className="resource_item">
            <a href="https://cn.vuejs.org/" target="_blank" rel="noopener noreferrer">Vue</a>
          </li>
          <li className="resource_item">
            <a href="https://angular.io/" target="_blank" rel="noopener noreferrer">Angular</a>
          </li>
          <li className="resource_item">
            <a href="http://www.bootcss.com/" target="_blank" rel="noopener noreferrer">Bootstrap</a>
          </li>
          <li className="resource_item">
            <a href="http://jquery.com/" target="_blank" rel="noopener noreferrer">Jquery</a>
          </li>
        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">CSS预处理器:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="http://www.sasschina.com/guide/" target="_blank" rel="noopener noreferrer">Sass</a>
          </li>
          <li className="resource_item">
            <a href="http://lesscss.org/" target="_blank" rel="noopener noreferrer">Less</a>
          </li>
          <li className="resource_item">
            <a href="http://stylus-lang.com/" target="_blank" rel="noopener noreferrer">Stylus</a>
          </li>
        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">前端构建工具:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="https://webpack.github.io/" target="_blank" rel="noopener noreferrer">Webpack</a>
          </li>
          <li className="resource_item">
            <a href="http://www.gulpjs.com.cn/" target="_blank" rel="noopener noreferrer">Gulp</a>
          </li>
          <li className="resource_item">
            <a href="http://wiki.jikexueyuan.com/list/front-end/" target="_blank" rel="noopener noreferrer">Grunt</a>
          </li>
          <li className="resource_item">
            <a href="http://yeoman.io/generators/" target="_blank" rel="noopener noreferrer">Yeoman</a>
          </li>
        </ul>
      </div>

      <div className="resource_part">
        <h2 className="resource_type">前端性能分析工具:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="https://developers.google.com/speed/pagespeed/insights/?hl=zh-CN" target="_blank" rel="noopener noreferrer">PageSpeed Insights</a>
          </li>
          <li className="resource_item">
            <a href="https://gtmetrix.com/" target="_blank" rel="noopener noreferrer">GTmetrix</a>
          </li>
          <li className="resource_item">
            <a href="http://www.webpagetest.org/" target="_blank" rel="noopener noreferrer">WebPageTest</a>
          </li>
        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">前端博客:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="http://www.ruanyifeng.com/blog/" target="_blank" rel="noopener noreferrer">阮一峰的网络日志</a>
          </li>
          <li className="resource_item">
            <a href="http://www.zhangxinxu.com/wordpress/" target="_blank" rel="noopener noreferrer">张鑫旭-鑫空间-鑫生活</a>
          </li>
          <li className="resource_item">
            <a href="http://lea.verou.me/" target="_blank" rel="noopener noreferrer">Lea Verou</a>
          </li>
        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">前端社区:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="http://www.w3cplus.com/" target="_blank" rel="noopener noreferrer">W3CPlus</a>
          </li>
          <li className="resource_item">
            <a href="https://www.w3ctech.com/" target="_blank" rel="noopener noreferrer">W3Ctech</a>
          </li>
          <li className="resource_item">
            <a href="http://www.css88.com/" target="_blank" rel="noopener noreferrer">Web前端开发</a>
          </li>

          <li className="resource_item">
            <a href="http://web.jobbole.com/" target="_blank" rel="noopener noreferrer">伯乐在线</a>
          </li>
          <li className="resource_item">
            <a href="https://weekly.75team.com/" target="_blank" rel="noopener noreferrer">奇舞周刊</a>
          </li>
          <li className="resource_item">
            <a href="https://css-tricks.com/" target="_blank" rel="noopener noreferrer">CSS-tricks</a>
          </li>

        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">前端书籍:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="https://book.douban.com/subject/10546125/" target="_blank" rel="noopener noreferrer">JavaScript高级程序设计（第3版）</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/6038371/" target="_blank" rel="noopener noreferrer">JavaScript DOM编程艺术（第2版）</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/3590768/" target="_blank" rel="noopener noreferrer">JavaScript语言精粹（修订版）</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/24744217/" target="_blank" rel="noopener noreferrer">JavaScript设计模式</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/26708954/" target="_blank" rel="noopener noreferrer">ES6标准入门(第2版)</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/4736167/" target="_blank" rel="noopener noreferrer">精通CSS：高级Web标准解决方案（第2版）</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/26745943/" target="_blank" rel="noopener noreferrer">CSS揭秘</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/24708139/" target="_blank" rel="noopener noreferrer">HTML5+CSS3从入门到精通</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/10792216/" target="_blank" rel="noopener noreferrer">锋利的jQuery（第2版）</a>
          </li>
          <li className="resource_item">
            <a href="http://wiki.jikexueyuan.com/list/front-end/" target="_blank" rel="noopener noreferrer">高性能网站建设指南：前端工程师技能精髓</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/3132277/" target="_blank" rel="noopener noreferrer">高性能网站建设进阶指南：Web开发者性能优化最佳实践</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/4719162/" target="_blank" rel="noopener noreferrer">编写高质量代码：Web前端开发修炼之道</a>
          </li>
          <li className="resource_item">
            <a href="https://book.douban.com/subject/20390374/" target="_blank" rel="noopener noreferrer">响应式Web设计 HTML5和CSS3实战（第2版）</a>
          </li>
        </ul>
      </div>
      <div className="resource_part">
        <h2 className="resource_type">其他:</h2>
        <ul className="resource_list">
          <li className="resource_item">
            <a href="http://www.jsonohyeah.com/" target="_blank" rel="noopener noreferrer">JSON在线mock</a>
          </li>
          <li className="resource_item">
            <a href="http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000" target="_blank" rel="noopener noreferrer">Git</a>
          </li>
          <li className="resource_item">
            <a href="http://caniuse.com/" target="_blank" rel="noopener noreferrer">CanIUse</a>
          </li>
          <li className="resource_item">
            <a href="http://cubic-bezier.com/" target="_blank" rel="noopener noreferrer">贝塞尔曲线</a>
          </li>
          <li className="resource_item">
            <a href="https://imageoptim.com/" target="_blank" rel="noopener noreferrer">图片压缩</a>
          </li>
          <li className="resource_item">
            <a href="http://caniuse.com/" target="_blank" rel="noopener noreferrer">Css sprites</a>
          </li>
          <li className="resource_item">
            <a href="http://cubic-bezier.com/" target="_blank" rel="noopener noreferrer">Iconfont</a>
          </li>
          <li className="resource_item">
            <a href="http://fontawesome.io/" target="_blank" rel="noopener noreferrer">Font Awesome</a>
          </li>
          <li className="resource_item">
            <a href="https://fonts.google.com/" target="_blank" rel="noopener noreferrer">Google fonts</a>
          </li>
          <li className="resource_item">
            <a href="http://www.dafont.com/" target="_blank" rel="noopener noreferrer">Dafont</a>
          </li>
          <li className="resource_item">
            <a href="https://typekit.com/" target="_blank" rel="noopener noreferrer">Typekit</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default Resource;
