import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'flux/utils';
import homeStore from '../js/store/homeStore';
import Nav from '../js/components/app/nav';
import TypeList from '../js/components/app/typeList';
import DateList from '../js/components/app/dateList';
import ArticleList from '../js/components/app/articleList';
import Tags from '../js/components/app/tags';
import API from '../js/api/homeAPI';
import pageActions from '../js/action/homeActions';

class app extends PureComponent {
  static getStores() {
    return [homeStore];
  }
  static calculateState() {
    return {
      info: homeStore.getState(),
    };
  }
  static propTypes = {
    location: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      info: homeStore.getState(),
      queryStr: props.location.query.query || '',
    };
  }
  componentWillMount() {
    // 使用缓存渲染页面
    const bigTypes = JSON.parse(localStorage.getItem('ARTICLE_TYPE')) || [];
    const newArticles = JSON.parse(localStorage.getItem('NEW_ARTICLE_LIST')) ? JSON.parse(localStorage.getItem('NEW_ARTICLE_LIST')).rows : [];
    const hotArticles = JSON.parse(localStorage.getItem('HOT_ARTICLE_LIST')) || [];
    const dateList = JSON.parse(localStorage.getItem('ARTICLE_DATE_LIST')) || [];
    const tagList = JSON.parse(localStorage.getItem('ARTICLE_TAG_LIST')) || [];
    pageActions.getStorageData({
      bigTypes, newArticles, hotArticles, dateList, tagList,
    });
  }
  componentDidMount() {
    // 获取文章类型列表
    API.getArticleAllTypes();
    // 获取最新文章列表
    API.getNewArticleList();
    // 获取最热文章列表
    API.getHotArticleList();
    // 获取文章时间归档列表
    API.getDateList();
    // 获取文章关键字列表
    API.getTagList();
  }
  componentWillReceiveProps(nextProps) {
    // url参数改变后,搜索框重新初始化
    this.setState({
      queryStr: nextProps.location.query.query || '',
    });
  }

  render() {
    const { info, queryStr } = this.state;
    return (
      <div>
        <Nav types={info.bigTypes} query={queryStr} />
        <div className="header_fix" />
        <div className="banner">
          <span className="banner_logo page_container">只希望自己的每次记录都有些许突破～～</span>
        </div>
        <div className="content page_container">
          <div className="side_left">
            {/* 文章类型分类 */}
            {info.bigTypes.length !== 0 && <TypeList title="分类目录" types={info.bigTypes} />}
            {/* 文章发布时间分类 */}
            {info.dateList.length !== 0 && <DateList title="文章归档" dates={info.dateList} />}
          </div>
          <div className="main">
            {this.props.children}
          </div>
          <div className="side_right">
            {info.newArticles.length !== 0 && <ArticleList
              title="最新文章"
              icon="icon-zuixin1"
              data={info.newArticles}
              type="new"
            />}
            {info.hotArticles.length !== 0 && <ArticleList
              title="最热文章"
              icon="icon-remen-copy"
              data={info.hotArticles}
              type="hot"
            />}
            {info.tagList.length !== 0 && <Tags title="标签" icon="icon-biaoqian" tags={info.tagList} />}
          </div>
        </div>
        <div className="to_top only_pc_show" onClick={() => window.scrollTo(0, 0)}>
          <i className="iconfont icon-icon22" />
        </div>
        <div className="footer">蜀ICP备15015945号-1 @Mia</div>
      </div>
    );
  }
}
export default Container.create(app);
