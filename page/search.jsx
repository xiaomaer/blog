import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'flux/utils';
import homeStore from '../js/store/homeStore';
import Pager from '../js/components/pager';
import Articles from '../js/components/articles';
import API from '../js/api/homeAPI';
import Loading from '../js/components/detail/Loading';

class Search extends PureComponent {
  static getStores() {
    return [homeStore];
  }
  static calculateState() {
    return {
      info: homeStore.getState(),
    };
  }
  static propTypes = {
    location: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      info: homeStore.getState(),
    };
    const queryStr = props.location.query.query;
    this.params = {
      userID: 2, // int,用户ID
      size: 20, // int,每页个数
      page: 1,
      cid: 0, // 全部文章
      query: queryStr, // 查询字符串
      date: '',
      tag: '',
      type_name: `“${queryStr}”的搜索结果`, // 文章列表名称
    };
  }
  componentWillMount() {
    // 初始化时，获取全部文章列表
    API.getArticleList(this.params);
  }
  componentWillReceiveProps(nextProps) {
    // url参数改变后，重新请求
    const queryStr = nextProps.location.query.query;
    const typeName = `"${queryStr}" 的搜索结果`;
    if (queryStr === this.props.location.query.query) return false;
    this.params = {
      ...this.params,
      page: 1,
      query: queryStr,
      type_name: typeName,
    };
    API.getArticleList(this.params);
    return true;
  }

  // 上一页
  prevPage = (e) => {
    e.stopPropagation();
    const { page } = this.params;
    if (page === 1) return;
    this.params = {
      ...this.params,
      page: page - 1,
    };
    window.scrollTo(0, 0);
    API.getArticleList(this.params);
  }
  // 下一页
  nextPage = (e) => {
    e.stopPropagation();
    const { page } = this.params;
    if (page === this.state.info.articles.page_total) return;
    this.params = {
      ...this.params,
      page: page + 1,
    };
    window.scrollTo(0, 0);
    API.getArticleList(this.params);
  }
  // 切换到指定页
  changePage = (e) => {
    const curr_page = e.target.dataset.id
      ? parseInt(e.target.dataset.id, 10)
      : 0;
    if (curr_page === this.params.page) return;
    if (curr_page !== 0) {
      this.params = {
        ...this.params,
        page: curr_page,
      };
      window.scrollTo(0, 0);
      API.getArticleList(this.params);
    }
  }
  render() {
    const { info } = this.state;
    return (
      <div>
        {/* 文章列表 */}
        {!info.isLoading && <Articles data={info.articles} />}
        {/* 分页组件 */}
        {!info.isLoading && info.articles.page_total !== 0 &&
          (<Pager
            pageTotal={info.articles.page_total}
            currNum={this.params.page}
            nextPage={this.nextPage}
            prevPage={this.prevPage}
            changePage={this.changePage}
          />)
        }
        <Loading isLoading={info.isLoading} />
      </div>
    );
  }
}
export default Container.create(Search);
