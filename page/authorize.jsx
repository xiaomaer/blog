import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import API from '../js/api/homeAPI';
import { setCookie } from '../js/common/utils';

export default class Authorize extends PureComponent {
  static propTypes = {
    location: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
    this.code = this.props.location.query.code;
  }
  componentWillMount() {
    const that = this;
    // 通过code请求换取用户授权的token
    API.getAccessToken(that.code, (data) => {
      // 通过cookie存储token，用于发表、回复评论
      const token = data.access_token;
      const expireDate = data.expires_in / 60 / 60 / 24; // token多少天过期
      setCookie('token', token, expireDate);
      // 关闭当前窗口
      window.close();
    });
  }
  render() {
    return (
      <div>这个页面用于畅言调用第三方登录，登录成功后，通过code获取畅言的token，用于调用其他接口</div>
    );
  }
}
