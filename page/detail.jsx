/* Created by xiaoma on 2017/3/3.
*/
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'flux/utils';
import homeStore from '../js/store/homeStore';
import API from '../js/api/homeAPI';
import { getCookie } from '../js/common/utils';
import Detail from '../js/components/detail/detail';
import Comment from '../js/components/detail/comment';
import CommentList from '../js/components/detail/commentList';
import LoginDialog from '../js/components/detail/loginDialog';
import Loading from '../js/components/detail/Loading';

class ArticleDetail extends PureComponent {
  static getStores() {
    return [homeStore];
  }
  static calculateState() {
    return {
      info: homeStore.getState(),
    };
  }
  static propTypes = {
    params: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      info: homeStore.getState(),
      showLogin: false,
    };
  }
  componentWillMount() {
    const { articleID } = this.props.params;
    // 获取文章详情
    API.getArticleDetail(articleID, location.href);
  }
  componentDidMount() {
    this.getCurrUserInfo();
  }

  componentWillReceiveProps(nextProps) {
    // url参数改变后，重新请求
    const { articleID } = nextProps.params;
    if (articleID === this.props.params.articleID) return false;
    // 获取文章详情
    API.getArticleDetail(articleID, location.href);
    return true;
  }
  getCurrUserInfo = () => {
    // 获取当前登录用户信息
    const token = getCookie('token');
    if (token && token !== 'undefined') {
      API.getCurrUserInfo(token);
    }
  }
  // 点击登录方式进行第三方登录
  login = (platformID) => {
    // 打开第三方登录对话框
    const winWidth = window.innerWidth || document.body.clientWidth || document.documentElement.clientWidth;
    const winHeight = window.innerHeight || document.body.clientHeight || document.documentElement.clientHeight;
    const left = (winWidth - 700) / 2;
    const top = (winHeight - 500) / 2;
    const features = `left=${left},top=${top},width=700,height=500,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no`;
    const win = window.open(
      `${'https://changyan.sohu.com/api/oauth2/authorize?client_id=cysaWS3bo&redirect_uri=' +
      'http://mia.dobit.top/authorize&response_type=code&platform_id='}${platformID}`,
      '', features,
    );
    // 监控window.open打开的页面是否关闭，如果关闭，则关闭登录弹窗
    const timer = setInterval(() => {
      if (win.closed) {
        clearInterval(timer);
        this.getCurrUserInfo();
        this.hideDialog();
      }
    }, 500);
  }
  submitComment = (content, reply_id) => {
    // 获取token
    const token = getCookie('token');
    // token不存在，弹出登录对话框
    if (!token || token === 'undefined') {
      this.setState({
        showLogin: true,
      });
      return;
    }
    // token存在，发表或回复评论
    const { topic_id } = this.state.info;
    API.submitComment(topic_id, content, reply_id, token);
  }
  hideDialog = () => {
    this.setState({
      showLogin: false,
    });
  }
  render() {
    const { info, showLogin } = this.state;
    return (
      <div className="articleInfo">
        {!info.isLoading && <Detail data={info.articleDetail} />}
        {!info.isLoading &&
          <div className="article_comment">
            {info.cmt_sum !== undefined && <h1 className="article_comment_title">{`评论（${info.cmt_sum}）`}</h1>}
            {info.cmt_sum !== undefined && info.commentIndex === -1 && <Comment onSubmit={this.submitComment} userInfo={info.user} commentState={info.commentState} />}
            <CommentList
              index={info.commentIndex}
              data={info.comments}
              userInfo={info.user}
              onSubmit={this.submitComment}
            />
          </div>}
        <LoginDialog isShow={showLogin} hideDialog={this.hideDialog} login={this.login} />
        <Loading isLoading={info.isLoading} />
      </div>
    );
  }
}
export default Container.create(ArticleDetail);
