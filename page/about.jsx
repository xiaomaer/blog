import React from 'react';

const About = () => (
  <div className="self">
    <h1 className="center_title">关于我</h1>
    <ul className="self_info">
      <li>昵称：小马</li>
      <li>英文名称：Mia</li>
      <li>原blog地址：<a href="http://y.dobit.top/">http://y.dobit.top/</a>
      </li>
      <li>gitlab：<a href="https://gitlab.com/xiaomaer">https://gitlab.com/xiaomaer</a>
      </li>
      <li>github：<a href="https://github.com/xiaomaer">https://github.com/xiaomaer</a>
      </li>
      <li>邮箱：meiying.mmy@foxmail.com</li>
    </ul>
  </div>
);


export default About;
