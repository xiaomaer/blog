/**
 * Created by xiaoma on 2017/3/3.
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'flux/utils';
import homeStore from '../js/store/homeStore';
import Pager from '../js/components/pager';
import Articles from '../js/components/articles';
import API from '../js/api/homeAPI';
import pageActions from '../js/action/homeActions';
import Loading from '../js/components/detail/Loading';

class Home extends PureComponent {
  static getStores() {
    return [homeStore];
  }
  static calculateState() {
    return {
      info: homeStore.getState(),
    };
  }
  static propTypes = {
    params: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      info: homeStore.getState(),
    };
    // 处理请求参数
    const {
      typeID = 0,
      dateTime = '',
      tagName = '',
    } = this.props.params;
    let { typeName = '全部文章' } = this.props.params;
    if (dateTime) { typeName = `${dateTime} 所有文章`; }
    if (tagName) { typeName = `${tagName} 相关文章`; }
    this.params = {
      userID: 2, // int,用户ID
      size: 20, // int,每页个数
      page: 1,
      cid: typeID,
      query: '',
      date: dateTime,
      tag: tagName,
      type_name: typeName,
    };
  }
  componentWillMount() {
    // 使用缓存渲染页面
    const articles = JSON.parse(localStorage.getItem('NEW_ARTICLE_LIST')) || {};
    articles.type_name = this.params.type_name;
    pageActions.getArticleList(articles);
    // 请求获取最新全部文章列表
    API.getArticleList(this.params);
  }
  componentWillReceiveProps(nextProps) {
    // url参数改变后，重新请求
    const {
      typeID = 0,
      dateTime = '',
      tagName = '',
    } = nextProps.params;
    let {
      typeName,
    } = nextProps.params;
    const prevParams = {
      typeID: 0, dateTime: '', tagName: '', ...this.props.params,
    };
    if (prevParams.typeID === typeID && prevParams.dateTime === dateTime && prevParams.tagName === tagName) return false;
    if (dateTime) { typeName = `${dateTime} 所有文章`; }
    if (tagName) { typeName = `${tagName} 相关文章`; }
    this.params = {
      ...this.params,
      page: 1,
      cid: typeID,
      type_name: typeName,
      date: dateTime,
      tag: tagName,
    };
    API.getArticleList(this.params);
    return true;
  }
  // 上一页
  prevPage = (e) => {
    e.stopPropagation();
    const { page } = this.params;
    if (page === 1) {
      return;
    }
    this.params = {
      ...this.params,
      page: page - 1,
    };
    window.scrollTo(0, 0);
    API.getArticleList(this.params);
  }
  // 下一页
  nextPage = (e) => {
    e.stopPropagation();
    const { page } = this.params;
    if (page === this.state.info.articles.page_total) return;
    this.params = {
      ...this.params,
      page: page + 1,
    };
    window.scrollTo(0, 0);
    API.getArticleList(this.params);
  }
  // 切换到指定页
  changePage = (e) => {
    const curr_page = e.target.dataset.id
      ? parseInt(e.target.dataset.id, 10)
      : 0;
    if (curr_page === this.params.page) return;
    if (curr_page !== 0) {
      this.params = {
        ...this.params,
        page: curr_page,
      };
      window.scrollTo(0, 0);
      API.getArticleList(this.params);
    }
  }
  render() {
    const { info } = this.state;
    return (
      <div>
        {/* 文章列表 */}
        {!info.isLoading && <Articles data={info.articles} />}
        {/* 分页组件 */}
        {!info.isLoading && info.articles.page_total && (<Pager
          pageTotal={info.articles.page_total}
          currNum={this.params.page}
          nextPage={this.nextPage}
          prevPage={this.prevPage}
          changePage={this.changePage}
        />)
        }
        <Loading isLoading={info.isLoading} />
      </div>
    );
  }
}
export default Container.create(Home);
